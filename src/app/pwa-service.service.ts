import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DexieService } from 'src/core/service/dexie.service';
import { RequestService } from 'src/core/service/request.service';
import Dexie from 'dexie';
import { MediaUpload } from 'src/core/model/media-upload.model';

@Injectable({
  providedIn: 'root'
})
export class PwaServiceService {

  //private url: string = 'https://carmen.serveo.net/api';
  private url: string = 'https://uat-api.openport.com/integration-api/service';

  private imageTable: Dexie.Table<MediaUpload, Number>;
  private documentTable: Dexie.Table<MediaUpload, Number>;

  constructor(private requestSvc: RequestService,
              private dexieSvc: DexieService) {

    this.imageTable = this.dexieSvc.table('images');
    this.documentTable = this.dexieSvc.table('documents');
  }

  public async imageList(): Promise<MediaUpload[]> {
    return await this.imageTable.toArray();
  }
  
  public async documentList(): Promise<MediaUpload[]> {
    return await this.documentTable.toArray();
  }
  
  public async postPing(ping: any) : Promise<string> {
 
    this.requestSvc.queue({
      url: `${this.url}/pwa-gps-ping`,
      method: 'POST',
      param: JSON.stringify(ping)
    });

    return "ok";

  }

  public async sendDeviceId(pushDevice: any) {
    this.requestSvc.queue({
      url: `${this.url}/pwa-gps-ping`,
      method: 'POST',
      param: JSON.stringify(pushDevice)
    });
    return "ok";
  }

  public async uploadImage(mediaUpload: any, type:string) {

    if(type === 'document')
      this.documentTable.add(mediaUpload);
    else
      this.imageTable.add(mediaUpload);

    this.requestSvc.queue({
      url: `${this.url}/pwa-media-upload`,
      method: 'POST',
      param: JSON.stringify(mediaUpload)
    });
    return "ok";
  }

  public async deleteMediaUpload(type: string) : Promise<void> {
    if(type === 'document') await this.documentTable.clear();
    else await this.imageTable.clear();
  }

}
