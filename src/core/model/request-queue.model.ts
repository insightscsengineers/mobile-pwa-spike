interface RequestQueue {
    id: number;
    method: string;
    url: string;
    param: string;
    headers: string;
    retry: number;
    status: string;
}